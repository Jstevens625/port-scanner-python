import sys, argparse, ipaddress, socket
from Scanner import *
from Setup import get_ips, get_ports
from subprocess import call

def main(Args):

    #Clear Screen for Program
    call('clear')

    ip_list = get_ips(Args)
    port_list = get_ports(Args)

    #for ip in ip_list:
    #    print(ip)

    #for port in port_list:
    #    print(port)

    if Args.port:
        # Port Scaning mode (Do port scanning memes)
        if Args.tcp:
            TcpPortScan(ip_list, port_list, Args.timeout, Args.verbose)
        elif Args.udp:
            UdpPortScan(ip_list, port_list, Args.timeout, Args.verbose)
        else:
            print("no scanning type specified")

    elif Args.ping:
        # Ping Mode
        pingscan(ip_list, Args.timeout, Args.verbose)
        #port_scan_output()


def ParseArgs():
    Args =  argparse.ArgumentParser(description="Network Scanner")


    mode = Args.add_mutually_exclusive_group()
    mode.add_argument('-ping', action='store_true',
                    help='Set mode to ping scan')
    mode.add_argument('-port', action='store_true',
                    help='set mode to port scan')

    port_scan_type = Args.add_mutually_exclusive_group()
    port_scan_type.add_argument('-tcp', action='store_true',
                    help='set scan to use TCP packets')
    port_scan_type.add_argument('-udp', action='store_true',
                    help='set scan to use UDP ')


    Args.add_argument('-ip', action='append', dest='ip_list',
                    help='Add a target ip (repeat to add multiple)')

    Args.add_argument('-hostname', action='append', dest='hostname_list',
                    help='Add a target hostname (repeat to add multiple)')

    Args.add_argument('-ipr', action='append', dest='ipr_list',
                    help='Add a range of target ips (repeat to add multiple)')

    Args.add_argument('-cidr', action='append', dest='cidr_list',
                    help='Add a range of target ips with CIDR (repeat to add multiple)')


    Args.add_argument('-pt', action='append', type = int, dest='port_list',
                    help='Add a port to scan (repeat to add multiple)')

    Args.add_argument('-pr', action='append', dest='portr_list',
                    help='Range of ports to scan (repeat to add multiple)')

    Args.add_argument('-commonports', action="store_true", dest="commonports", default=False,
                    help='Add common ports')

    Args.add_argument('-timeout', action="store", dest="timeout", type=float, default=0.5,
                    help='sets the timeout for scanning')


    Args.add_argument('-v', action="store_true", dest="verbose", default=False,
                    help='set output to verbose')

    return Args.parse_args()


if __name__ == "__main__":
    main(ParseArgs())
