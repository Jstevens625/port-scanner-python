import ipaddress, socket

def get_ips(Args):

    if Args.ip_list:
        ip_list = Args.ip_list
    else:
        ip_list = []

    if Args.hostname_list:
        for hostname in Args.hostname_list:
            ip_list.append(socket.gethostbyname(hostname))

    if Args.ipr_list:
        for ip_range in Args.ipr_list:
            get_ip_range(ip_range, ip_list)

    if Args.cidr_list:
        for cidr in Args.cidr_list:
            get_cidr_ip_range(cidr, ip_list)

    return list(set(ip_list))


def get_ip_range(ip_range, ip_list):
    split_ip_range = ip_range.split('.')

    octet_range = []

    for octet in split_ip_range:
        current_octet_range = []
        split_octet = octet.split('-')
        if len(split_octet) > 1:
            for x in range(int(split_octet[0]), int(split_octet[1])+1):
                current_octet_range.append(str(x))
        else:
            current_octet_range.append(split_octet[0])

        octet_range.append(current_octet_range)

    for first_octet in octet_range[0]:
        for second_octet in octet_range[1]:
            for third_octet in octet_range[2]:
                for fourth_octet in octet_range[3]:
                    ip_list.append(first_octet + "." + second_octet + "." + third_octet + "." + fourth_octet)


def get_cidr_ip_range(cidr, ip_list):
    for  ip in ipaddress.IPv4Network(cidr):
        ip_list.append(str(ip))


def get_ports(Args):
    if Args.port_list:
        port_list = Args.port_list
    else:
        port_list = []

    if Args.commonports:
        # FTP (File Transfer Protocol)
        port_list.append(21)
        # SSH (Secure Shell)
        port_list.append(22)
        # DNS (Domain Name System)
        port_list.append(23)
        # SMTP (Simple Mail Transfer Protocol)
        port_list.append(25)
        # DNS (Domain Name System)
        port_list.append(53)
        # HTTP
        port_list.append(80)
        # HTTP (Hypertext Transport Protocol) and HTTPS (HTTP over SSL)
        port_list.append(443)
        # POP3 (Post Office Protocol version 3)
        port_list.append(110)
        # Windows RPC
        port_list.append(135)
        # Windows NetBIOS over TCP/IP
        port_list.append(137)
        port_list.append(138)
        port_list.append(139)
        # Microsoft SQL Server
        port_list.append(1433)
        port_list.append(1434)
        # plex
        port_list.append(32400)

    if Args.portr_list:
        for port_range in Args.portr_list:
            range_list = port_range.split('-')
            for x in range(int(range_list[0]), int(range_list[1])+1):
                port_list.append(int(x))


    return list(set(port_list))
