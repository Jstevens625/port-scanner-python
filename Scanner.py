from __future__ import print_function
import socket, argparse, sys, os, progressbar, time
from ping3 import ping

#Easy Formatting
def separation():
        print('+' * 142)

#Use TCP to Port Scan
def TcpPortScan(ip_list, port_list, scan_timeout, verbose):

    for ip in ip_list:
        bar_prog = 0
        with progressbar.ProgressBar(max_value=len(port_list), redirect_stdout=True) as bar:
            try:
                print("Scanning ip: " + ip)
                bar.update(bar_prog)
                for port in port_list:
                    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    sock.settimeout(scan_timeout)
                    res = sock.connect_ex((ip,port))
                    if res == 0:
                        print("Port ",port,":     Open")
                    elif verbose:
                        print("Port ",port,":     Closed")
                    sock.close()
                    bar_prog += 1
                    bar.update(bar_prog)
            except KeyboardInterrupt:
                print("\nYou canceled the program. Exiting now...")
                sys.exit()
            except socket.gaierror:
                print("Hostname could not be resolved. Exiting now...")
                sys.exit()

        print("\n")


#Use UDP to Port Scan
def UdpPortScan(ip_list, port_list, scan_timeout, verbose):

    for ip in ip_list:
        bar_prog = 0
        with progressbar.ProgressBar(max_value=len(port_list), redirect_stdout=True) as bar:
            try:
                print("Scanning ip: " + ip)
                for port in port_list:
                    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                    sock.settimeout(scan_timeout)
                    res = sock.connect_ex((ip,port))
                    if res == 0:
                        print("Port ",port,":     Open")
                    elif verbose:
                        print("Port ",port,":     Closed")
                    sock.close()
                    bar_prog += 1
                    bar.update(bar_prog)
            except KeyboardInterrupt:
                print("You canceled the program exiting now...")
                sys.exit()
            except socket.gaierror:
                print("Hostname could not be resolved. Exiting now...")
                sys.exit()

        print("\n")

#Pinging
def pingscan(ip_list, ping_timeout, verbose):
        ip_count = 0
        with progressbar.ProgressBar(max_value=len(ip_list), redirect_stdout=True) as bar:
            for ip in ip_list:
                if ping(ip, timeout=ping_timeout):
                    print(ip + " - is up!")
                elif verbose:
                    print(ip + " - did not respond in time :(")
                ip_count += 1
                bar.update(ip_count)
                time.sleep(0.1)
